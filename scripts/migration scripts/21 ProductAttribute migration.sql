USE spheres1
DECLARE @TableName varchar(100) = 'ProductAttribute';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(100),
	[Description] NVARCHAR(MAX)
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(100),
	[Description] NVARCHAR(MAX)
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id, Name, [Description])
SELECT Id, Name, [Description] 
FROM [spheres1].dbo.ProductAttribute

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);

SELECT @currentElementId = Id,
	   @currentElementName = Name
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
	
	INSERT INTO [hpl2].dbo.ProductAttribute ( Name, [Description] )
	SELECT Name, [Description] FROM @currentElement
	
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'