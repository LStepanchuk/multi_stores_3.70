USE spheres1
DECLARE @TableName varchar(100) = 'Customer_CustomerRole_Mapping';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	Customer_Id INT,	
	CustomerRole_Id INT
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	Customer_Id INT,	
	CustomerRole_Id INT
	---IMPORTANT
);

INSERT INTO @AllElements
        (Customer_Id, CustomerRole_Id)
SELECT Customer_Id, CustomerRole_Id
FROM [spheres1].dbo.Customer_CustomerRole_Mapping

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentCustomerId INT;
DECLARE @currentRoleId INT;

SELECT @currentCustomerId = Customer_Id,
	   @currentRoleId = CustomerRole_Id
FROM @currentElement

BEGIN
	DECLARE @currentCustomerIdInNewTable INT;
	DECLARE @currentRoleIdInNewTable INT;
	
	---GET Customer
	SELECT @currentCustomerIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentCustomerId AND TableName = 'Customer'
	IF (@currentCustomerIdInNewTable IS NULL) AND (@currentCustomerId IS NOT NULL)
		PRINT 'New Customer is not found'

	---GET Role
	SELECT @currentRoleIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentRoleId AND TableName = 'CustomerRole'
	IF (@currentRoleIdInNewTable IS NULL) AND (@currentRoleId IS NOT NULL)
		PRINT 'New CustomerRole is not found'

	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	
	INSERT INTO [hpl2].dbo.Customer_CustomerRole_Mapping ( CustomerRole_Id, Customer_Id ) 
	VALUES (@currentRoleIdInNewTable, @currentCustomerIdInNewTable)
				
	---IMPORTANT

	DELETE FROM @AllElements
	WHERE @currentCustomerId = Customer_Id AND @currentRoleId = CustomerRole_Id
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'