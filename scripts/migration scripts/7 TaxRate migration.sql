USE [spheres1]
DECLARE @TableName varchar(20) = 'TaxRate';  
DECLARE @CurrentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));  
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	[Id] INT,
	[TaxCategoryId] INT,
	[CountryId] INT,
	[StateProvinceId] INT,
	[Zip] NVARCHAR(MAX),
	[Percentage] DECIMAL(18, 4),
	[StoreId] INT
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[TaxCategoryId] INT,
	[CountryId] INT,
	[StateProvinceId] INT,
	[Zip] NVARCHAR(MAX),
	[Percentage] DECIMAL(18, 4),
	[StoreId] INT
);

INSERT INTO @AllElements ( Id, TaxCategoryId, CountryId, StateProvinceId, Zip, Percentage, StoreId )
SELECT Id, TaxCategoryId, CountryId, StateProvinceId, Zip, Percentage, StoreId FROM [spheres1].dbo.TaxRate

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL
PRINT N'Start new iteration';  

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentCountryId INT,
		@currentStateId INT,
		@currentTaxCategory INT;

SELECT 
	 @currentElementId = Id, 
	 @currentCountryId = CountryId,
	 @currentStateId = StateProvinceId,
	 @currentTaxCategory = TaxCategoryId 
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
			@taxCategoryInNewTable INT,
			@countryIdInNewTable INT,
			@stateIdInNewTable INT;

	---GET TAX CATEGORY
	SELECT @taxCategoryInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentTaxCategory AND TableName = 'TaxCategory';

	---GET COUNTRY
	SELECT @countryIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentCountryId AND TableName = 'Country';

	---GET STATE
	SELECT @stateIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentStateId AND TableName = 'StateProvince'
	

	---IMPORTANT---MUST BE EDITED
	IF EXISTS(SELECT * FROM [hpl2].dbo.TaxRate WHERE TaxCategoryId = @taxCategoryInNewTable AND CountryId = @countryIdInNewTable AND StateProvinceId = @stateIdInNewTable)
	BEGIN
		SELECT @currentElementIdInNewTable = Id 
		FROM [hpl2].dbo.TaxRate 
		WHERE TaxCategoryId = @taxCategoryInNewTable AND 
			  CountryId = @countryIdInNewTable AND 
			  StateProvinceId = @stateIdInNewTable
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.TaxRate (TaxCategoryId, CountryId, StateProvinceId, Zip, Percentage, StoreId)
		SELECT  @taxCategoryInNewTable, @countryIdInNewTable, @stateIdInNewTable, Zip, Percentage, @CurrentStoreId FROM @currentElement
		
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END


	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'