DECLARE @TableName varchar(20) = 'Download';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START TO MIGRATE ALL Downloads'
BEGIN

DECLARE @AllDownloads TABLE
(
    [Id] INT,
	[UseDownloadUrl] BIT,
	[DownloadUrl] NVARCHAR(MAX),
	[DownloadBinary] VARBINARY(MAX),
	[ContentType] NVARCHAR(MAX),
	[Filename] NVARCHAR(MAX),
	[Extension] NVARCHAR(MAX),
	[IsNew] BIT,
	[DownloadGuid] UNIQUEIDENTIFIER
);

DECLARE @currentDownload TABLE
(
	[Id] INT,
	[UseDownloadUrl] BIT,
	[DownloadUrl] NVARCHAR(MAX),
	[DownloadBinary] VARBINARY(MAX),
	[ContentType] NVARCHAR(MAX),
	[Filename] NVARCHAR(MAX),
	[Extension] NVARCHAR(MAX),
	[IsNew] BIT,
	[DownloadGuid] UNIQUEIDENTIFIER
);

INSERT INTO @AllDownloads
        ( Id ,
          UseDownloadUrl ,
          DownloadUrl ,
          DownloadBinary ,
          ContentType ,
          [Filename] ,
          Extension ,
          IsNew ,
          DownloadGuid
        )
SELECT Id,
          UseDownloadUrl ,
          DownloadUrl ,
          DownloadBinary ,
          ContentType ,
          [Filename] ,
          Extension ,
          IsNew ,
          DownloadGuid
FROM [spheres1].dbo.Download

PRINT N'START LOOP';
 
StartDownloadsLoop:
PRINT N'Start new iteration';  

---GET FIRST ProductTemplate
DELETE FROM @currentDownload
INSERT INTO @currentDownload
SELECT TOP 1 * FROM @AllDownloads

DECLARE @currentDownloadId INT;
SELECT @currentDownloadId = Id FROM @currentDownload

BEGIN
	DECLARE @downloadInNewTable INT;

	INSERT INTO [hpl2].dbo.Download
	        ( UseDownloadUrl ,
	          DownloadUrl ,
	          DownloadBinary ,
	          ContentType ,
	          [Filename] ,
	          Extension ,
	          IsNew ,
	          DownloadGuid
	        )	
	SELECT UseDownloadUrl ,
	       DownloadUrl ,
	       DownloadBinary ,
	       ContentType ,
	       [Filename] ,
	       Extension ,
	       IsNew ,
	       DownloadGuid FROM @currentDownload
	
	SET @downloadInNewTable = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage (OldId, [NewId], TableName)
	VALUES (@currentDownloadId, @downloadInNewTable, @TableName)

	DELETE FROM @AllDownloads
	WHERE Id = @currentDownloadId
END

IF EXISTS (SELECT * FROM @AllDownloads)
	GOTO StartDownloadsLoop

END
PRINT N'PICTURES MIGRATION FINISHED'
