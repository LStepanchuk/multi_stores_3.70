USE [spheres1]
DECLARE @TableName varchar(100) = 'ProductTemplate';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	[Id] INT,
	[Name] NVARCHAR(100),
	[ViewPath] NVARCHAR(400),
	[DisplayOrder] INT
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[Name] NVARCHAR(100),
	[ViewPath] NVARCHAR(400),
	[DisplayOrder] INT
);

INSERT INTO @AllElements ( Id, Name, ViewPath, DisplayOrder )
SELECT Id, Name, ViewPath, DisplayOrder 
FROM [spheres1].dbo.ProductTemplate

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentViewPath NVARCHAR(400),
		@currentName NVARCHAR(400);

SELECT  @currentElementId = Id,
		@currentViewPath = ViewPath,
		@currentName = Name
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.ProductTemplate WHERE Name = @currentName AND ViewPath = @currentViewPath)
	BEGIN
		SELECT @currentElementIdInNewTable = Id FROM [hpl2].dbo.ProductTemplate
		WHERE Name = @currentName AND ViewPath = @currentViewPath
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.ProductTemplate ( Name, ViewPath, DisplayOrder )
		SELECT Name, ViewPath, DisplayOrder FROM @currentElement
		
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END
	
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'