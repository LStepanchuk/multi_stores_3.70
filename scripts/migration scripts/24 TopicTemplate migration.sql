USE spheres1
DECLARE @TableName varchar(100) = 'TopicTemplate';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'

DECLARE @AllElements TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[ViewPath] NVARCHAR(400),
	[DisplayOrder] INT
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[ViewPath] NVARCHAR(400),
	[DisplayOrder] INT
);

INSERT INTO @AllElements
        ( Id, Name, ViewPath, DisplayOrder )

SELECT Id, Name, ViewPath, DisplayOrder
FROM [spheres1].dbo.TopicTemplate

PRINT N'START LOOP';
WHILE EXISTS (SELECT * FROM @AllElements)
BEGIN
	---GET FIRST ELEMENT
	DELETE FROM @currentElement
	INSERT INTO @currentElement
	SELECT TOP 1 * FROM @AllElements

	---CURRENT ELEMENT FIELDS
	DECLARE @currentElementId INT,
			@currentViewPath NVARCHAR(400)

	---CURRENT ELEMENT FIELDS THAT ALREADY MIGRATED
	DECLARE @currentElementId_InNewDB INT

	SELECT  @currentElementId = Id,
		    @currentViewPath = ViewPath
	FROM    @currentElement
	
	---MAIN SECTION	

	IF EXISTS(SELECT * FROM [hpl2].dbo.TopicTemplate WHERE ViewPath = @currentViewPath)
	BEGIN
		SELECT @currentElementId_InNewDB = Id FROM [hpl2].dbo.TopicTemplate WHERE ViewPath = @currentViewPath
	END
	ELSE
	BEGIN
		 INSERT INTO [hpl2].dbo.TopicTemplate
		         ( Name, ViewPath, DisplayOrder )
		 SELECT Name, ViewPath, DisplayOrder FROM @currentElement
		 SET @currentElementId_InNewDB = SCOPE_IDENTITY();
	END
	
	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementId_InNewDB, @TableName)	
		
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

PRINT N'MIGRATION FINISHED ['+ @TableName +']'