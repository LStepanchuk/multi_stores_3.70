USE spheres1
DECLARE @TableName varchar(100) = 'CustomerRole';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[Name] [NVARCHAR](255),
	[FreeShipping] [BIT],
	[TaxExempt] [BIT],
	[Active] [BIT],
	[IsSystemRole] [BIT],
	[SystemName] [NVARCHAR](255),
	[PurchasedWithProductId] [INT]
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[Name] [NVARCHAR](255),
	[FreeShipping] [BIT],
	[TaxExempt] [BIT],
	[Active] [BIT],
	[IsSystemRole] [BIT],
	[SystemName] [NVARCHAR](255),
	[PurchasedWithProductId] [INT]
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          Name ,
          FreeShipping ,
          TaxExempt ,
          Active ,
          IsSystemRole ,
          SystemName ,
          PurchasedWithProductId
        )
SELECT Id ,
          Name ,
          FreeShipping ,
          TaxExempt ,
          Active ,
          IsSystemRole ,
          SystemName ,
          PurchasedWithProductId
FROM [spheres1].dbo.CustomerRole

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);

SELECT @currentElementId = Id,
	   @currentElementName = SystemName
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.CustomerRole WHERE SystemName = @currentElementName)
	BEGIN
		SELECT @currentElementIdInNewTable = Id 
		FROM [hpl2].dbo.CustomerRole 
		WHERE SystemName = @currentElementName
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.CustomerRole
		        ( Name ,
		          FreeShipping ,
		          TaxExempt ,
		          Active ,
		          IsSystemRole ,
		          SystemName ,
		          PurchasedWithProductId
		        )
		SELECT  Name ,
		          FreeShipping ,
		          TaxExempt ,
		          Active ,
		          IsSystemRole ,
		          SystemName ,
		          PurchasedWithProductId FROM @currentElement
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END		
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'