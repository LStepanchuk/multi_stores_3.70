USE [spheres1]
DECLARE @TableName varchar(100) = 'Address';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[FirstName] NVARCHAR(MAX),
	[LastName] NVARCHAR(MAX),
	[Email] NVARCHAR(MAX),
	[Company] NVARCHAR(MAX),
	[CountryId] INT,
	[StateProvinceId] INT,
	[City] NVARCHAR(MAX),
	[Address1] NVARCHAR(MAX),
	[Address2] NVARCHAR(MAX),
	[ZipPostalCode] NVARCHAR(MAX),
	[PhoneNumber] NVARCHAR(MAX),
	[FaxNumber] NVARCHAR(MAX),
	[CreatedOnUtc] DATETIME,
	[CustomAttributes] NVARCHAR(MAX)
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[FirstName] NVARCHAR(MAX),
	[LastName] NVARCHAR(MAX),
	[Email] NVARCHAR(MAX),
	[Company] NVARCHAR(MAX),
	[CountryId] INT,
	[StateProvinceId] INT,
	[City] NVARCHAR(MAX),
	[Address1] NVARCHAR(MAX),
	[Address2] NVARCHAR(MAX),
	[ZipPostalCode] NVARCHAR(MAX),
	[PhoneNumber] NVARCHAR(MAX),
	[FaxNumber] NVARCHAR(MAX),
	[CreatedOnUtc] DATETIME,
	[CustomAttributes] NVARCHAR(MAX)
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id, FirstName, LastName, Email, Company, CountryId, StateProvinceId, City, Address1, Address2, ZipPostalCode, PhoneNumber, FaxNumber, CreatedOnUtc, CustomAttributes )

SELECT Id, FirstName, LastName, Email, Company, CountryId, StateProvinceId, City, Address1, Address2, ZipPostalCode, PhoneNumber, FaxNumber, CreatedOnUtc, CustomAttributes
FROM [spheres1].dbo.[Address]

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
@currentCountryId INT,
@currentStateId INT;

SELECT @currentElementId = Id,
@currentCountryId = CountryId,
@currentStateId = StateProvinceId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT, @countryIdInNewTable INT, @stateIdInNewTable INT
		
	---GET COUNTRY
	IF @currentCountryId IS NULL
		SET @countryIdInNewTable = NULL
	ELSE
		SELECT @countryIdInNewTable = [NewId] FROM dbo.OldElementsStorage
		WHERE OldId = @currentCountryId AND TableName = 'Country';

	---GET STATE
	IF @currentStateId IS NULL
		SET @currentStateId = NULL
	ELSE
		SELECT @stateIdInNewTable = [NewId] FROM dbo.OldElementsStorage
		WHERE OldId = @currentStateId AND TableName = 'StateProvince'

	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.[Address] ( FirstName, LastName, Email, Company, CountryId, StateProvinceId, City, Address1, Address2, ZipPostalCode, PhoneNumber, FaxNumber, CreatedOnUtc, CustomAttributes )
	SELECT  FirstName, LastName, Email, Company, @countryIdInNewTable, @stateIdInNewTable, City, Address1, Address2, ZipPostalCode, PhoneNumber, FaxNumber, CreatedOnUtc, CustomAttributes FROM @currentElement
		
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
EndLoop:

PRINT N'MIGRATION FINISHED ['+ @TableName +']'