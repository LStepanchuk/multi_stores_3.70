DECLARE @TableName varchar(100) = 'DeliveryDate';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(100),	
	[DisplayOrder] INT
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(100),	
	[DisplayOrder] INT
	---IMPORTANT
);

INSERT INTO @AllElements ( Id, Name, DisplayOrder )
SELECT Id, Name, DisplayOrder
FROM [spheres1].dbo.DeliveryDate

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);

SELECT @currentElementId = Id, @currentElementName = Name
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.DeliveryDate WHERE Name = @currentElementName)
	BEGIN
		SELECT @currentElementIdInNewTable = Id FROM [hpl2].dbo.DeliveryDate
		WHERE Name = @currentElementName
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.DeliveryDate ( Name, DisplayOrder )		
		SELECT Name, DisplayOrder FROM @currentElement
		
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'