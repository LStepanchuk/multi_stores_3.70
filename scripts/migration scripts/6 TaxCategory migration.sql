USE [spheres1]
DECLARE @TableName varchar(20) = 'TaxCategory';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[DisplayOrder] INT
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[DisplayOrder] INT
);

INSERT INTO @AllElements (Id, Name, DisplayOrder)
SELECT Id, Name, DisplayOrder FROM [spheres1].dbo.TaxCategory

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL
PRINT N'Start new iteration';  

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);
SELECT @currentElementId = Id, @currentElementName = Name FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT;

	---IMPORTANT---MUST BE EDITED
	
	IF EXISTS (SELECT TOP 1 * FROM [hpl2].dbo.TaxCategory WHERE Name = @currentElementName)
	BEGIN
		SELECT TOP 1 @currentElementIdInNewTable = Id FROM [hpl2].dbo.TaxCategory
		WHERE Name = @currentElementName
	END
	ELSE
	BEGIN 
	    ---INSERT NEW ELEMENT INTO TARGET TABLE
		INSERT INTO [hpl2].dbo.TaxCategory ( Name, DisplayOrder )	
		SELECT  Name, DisplayOrder FROM @currentElement
		
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'