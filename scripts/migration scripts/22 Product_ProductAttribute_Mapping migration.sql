USE spheres1
DECLARE @TableName varchar(100) = 'Product_ProductAttribute_Mapping';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[ProductAttributeId] [INT],
	[TextPrompt] [NVARCHAR](MAX),
	[IsRequired] [BIT],
	[AttributeControlTypeId] [INT],
	[DisplayOrder] [INT],
	[ProductId] [INT],
	[ValidationMinLength] [INT],
	[ValidationMaxLength] [INT],
	[ValidationFileAllowedExtensions] [NVARCHAR](MAX),
	[ValidationFileMaximumSize] [INT],
	[DefaultValue] [NVARCHAR](MAX),
	[ConditionAttributeXml] [NVARCHAR](MAX)
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[ProductAttributeId] [INT],
	[TextPrompt] [NVARCHAR](MAX),
	[IsRequired] [BIT],
	[AttributeControlTypeId] [INT],
	[DisplayOrder] [INT],
	[ProductId] [INT],
	[ValidationMinLength] [INT],
	[ValidationMaxLength] [INT],
	[ValidationFileAllowedExtensions] [NVARCHAR](MAX),
	[ValidationFileMaximumSize] [INT],
	[DefaultValue] [NVARCHAR](MAX),
	[ConditionAttributeXml] [NVARCHAR](MAX)
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          ProductAttributeId ,
          TextPrompt ,
          IsRequired ,
          AttributeControlTypeId ,
          DisplayOrder ,
          ProductId ,
          ValidationMinLength ,
          ValidationMaxLength ,
          ValidationFileAllowedExtensions ,
          ValidationFileMaximumSize ,
          DefaultValue ,
          ConditionAttributeXml
        )
SELECT Id ,
          ProductAttributeId ,
          TextPrompt ,
          IsRequired ,
          AttributeControlTypeId ,
          DisplayOrder ,
          ProductId ,
          ValidationMinLength ,
          ValidationMaxLength ,
          ValidationFileAllowedExtensions ,
          ValidationFileMaximumSize ,
          DefaultValue ,
          ConditionAttributeXml
FROM [spheres1].dbo.Product_ProductAttribute_Mapping

PRINT N'START LOOP';
WHILE EXISTS (SELECT * FROM @AllElements)
BEGIN
	---GET FIRST ELEMENT
	DELETE FROM @currentElement
	INSERT INTO @currentElement
	SELECT TOP 1 * FROM @AllElements

	---CURRENT ELEMENT FIELDS
	DECLARE @currentElementId INT,
			@currentProductAttributeId INT,
			@currentProductId INT;

	---CURRENT ELEMENT FIELDS THAT ALREADY MIGRATED
	DECLARE @currentElementId_InNewDB INT,
			@currentProductAttributeId_InNewDB INT,
			@currentProductId_InNewDB INT;

	SELECT  @currentElementId = Id,
		    @currentProductAttributeId = ProductAttributeId,
			@currentProductId = ProductId
	FROM    @currentElement
	
	---MAIN SECTION	

	---GET PRODUCT
	SELECT @currentProductId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentProductId AND TableName = 'Product'
	IF (@currentProductId_InNewDB IS NULL) AND (@currentProductId IS NOT NULL)
		PRINT 'New Product is not found'

	---GET PRODUCT ATTRIBUTE
	SELECT @currentProductAttributeId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentProductAttributeId AND TableName = 'ProductAttribute'
	IF (@currentProductAttributeId_InNewDB IS NULL) AND (@currentProductAttributeId IS NOT NULL)
		PRINT 'New ProductAttribute is not found'

	INSERT INTO [hpl2].dbo.Product_ProductAttribute_Mapping
	        ( ProductAttributeId ,
	          TextPrompt ,
	          IsRequired ,
	          AttributeControlTypeId ,
	          DisplayOrder ,
	          ProductId ,
	          ValidationMinLength ,
	          ValidationMaxLength ,
	          ValidationFileAllowedExtensions ,
	          ValidationFileMaximumSize ,
	          DefaultValue ,
	          ConditionAttributeXml
	        )
	SELECT	@currentProductAttributeId_InNewDB ,
	          TextPrompt ,
	          IsRequired ,
	          AttributeControlTypeId ,
	          DisplayOrder ,
	          @currentProductId_InNewDB ,
	          ValidationMinLength ,
	          ValidationMaxLength ,
	          ValidationFileAllowedExtensions ,
	          ValidationFileMaximumSize ,
	          DefaultValue ,
	          ConditionAttributeXml
	FROM @currentElement
	
	SET @currentElementId_InNewDB = SCOPE_IDENTITY();
	

	---INSERT MAPPING IN TEMPORARY TABLE
	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementId_InNewDB, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

PRINT N'MIGRATION FINISHED ['+ @TableName +']'