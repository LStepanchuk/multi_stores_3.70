USE spheres1
DECLARE @TableName varchar(100) = 'StateProvince';  
DECLARE @CurrentStoreId INT = 2;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	[Id] INT,	
	[CountryId] INT,
	[Name] NVARCHAR(100),
	[Abbreviation] NVARCHAR(100),
	[Published] BIT,
	[DisplayOrder] INT
);

DECLARE @currentElement TABLE
(
	[Id] INT,	
	[CountryId] INT,
	[Name] NVARCHAR(100),
	[Abbreviation] NVARCHAR(100),
	[Published] BIT,
	[DisplayOrder] INT
);

INSERT INTO @AllElements ( Id, CountryId, Name, Abbreviation, Published, DisplayOrder )
SELECT Id, CountryId, Name, Abbreviation, Published, DisplayOrder 
FROM [spheres1].dbo.StateProvince

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementCountryId INT;
DECLARE @currentStateName NVARCHAR(100);


SELECT  @currentElementId = Id,
		@currentElementCountryId = CountryId,
		@currentStateName = Name
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT

	---GET COUNTRY
	DECLARE @countryInNewTable INT;
	SELECT @countryInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE TableName = 'Country' AND OldId = @currentElementCountryId


	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.StateProvince WHERE Name = @currentStateName AND CountryId = @countryInNewTable)
	BEGIN
		SELECT @currentElementIdInNewTable = Id 
		FROM [hpl2].dbo.StateProvince 
		WHERE Name = @currentStateName AND CountryId = @countryInNewTable
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.StateProvince ( CountryId, Name, Abbreviation, Published, DisplayOrder )
		SELECT  @countryInNewTable, Name, Abbreviation, Published, DisplayOrder FROM @currentElement

		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END		
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'