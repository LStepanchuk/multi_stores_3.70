USE [spheres1]
DECLARE @TableName varchar(20) = 'Picture';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )

PRINT N'START TO MIGRATE ALL PICTURES'
BEGIN

DECLARE @AllPictures TABLE
(
	[Id] INT,
	[PictureBinary] VARBINARY(MAX),
	[MimeType] NVARCHAR(40),
	[IsNew] BIT,
	[SeoFilename] NVARCHAR(300),
	[TitleAttribute] NVARCHAR(MAX),
	[AltAttribute] NVARCHAR(MAX)
);

DECLARE @currentPicture TABLE
(
	[Id] INT,
	[PictureBinary] VARBINARY(MAX),
	[MimeType] NVARCHAR(40),
	[IsNew] BIT,
	[SeoFilename] NVARCHAR(300),
	[TitleAttribute] NVARCHAR(MAX),
	[AltAttribute] NVARCHAR(MAX)
);

INSERT INTO @AllPictures (Id, PictureBinary, MimeType, IsNew, SeoFilename, TitleAttribute, AltAttribute)
SELECT Id, PictureBinary, MimeType, IsNew, SeoFilename, TitleAttribute, AltAttribute FROM [spheres1].dbo.Picture

PRINT N'START LOOP';
 
StartPictureLoop:
PRINT N'Start new iteration';  

---GET FIRST ProductTemplate
DELETE FROM @currentPicture
INSERT INTO @currentPicture
SELECT TOP 1 * FROM @AllPictures

DECLARE @currentPictureId INT;
SELECT @currentPictureId = Id FROM @currentPicture

BEGIN
	DECLARE @pictureInNewTable INT;

	INSERT INTO [hpl2].dbo.Picture (PictureBinary, MimeType, IsNew, SeoFilename, TitleAttribute, AltAttribute)
	SELECT PictureBinary, MimeType, IsNew, SeoFilename, TitleAttribute, AltAttribute FROM @currentPicture
	
	SET @pictureInNewTable = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage (OldId, [NewId], TableName)
	VALUES (@currentPictureId, @pictureInNewTable, @TableName)

	DELETE FROM @AllPictures
	WHERE Id = @currentPictureId
END

IF EXISTS (SELECT * FROM @AllPictures)
	GOTO StartPictureLoop

END
PRINT N'PICTURES MIGRATION FINISHED'
 