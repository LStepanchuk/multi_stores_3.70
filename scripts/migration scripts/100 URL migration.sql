USE spheres1
DECLARE @TableName varchar(100) = 'UrlRecord';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'

DECLARE @AllElements TABLE
(
	[Id] [INT],
	[EntityId] [INT],
	[EntityName] [NVARCHAR](400),
	[Slug] [NVARCHAR](400),
	[LanguageId] [INT],
	[IsActive] [BIT]
);

DECLARE @currentElement TABLE
(
	[Id] [INT],
	[EntityId] [INT],
	[EntityName] [NVARCHAR](400),
	[Slug] [NVARCHAR](400),
	[LanguageId] [INT],
	[IsActive] [BIT]
);

INSERT INTO @AllElements
        ( Id ,
          EntityId ,
          EntityName ,
          Slug ,
          LanguageId ,
          IsActive
        )
SELECT Id ,
          EntityId ,
          EntityName ,
          Slug ,
          LanguageId ,
          IsActive
FROM [spheres1].dbo.UrlRecord

PRINT N'START LOOP';
WHILE EXISTS (SELECT * FROM @AllElements)
BEGIN
	---GET FIRST ELEMENT
	DELETE FROM @currentElement
	INSERT INTO @currentElement
	SELECT TOP 1 * FROM @AllElements

	---CURRENT ELEMENT FIELDS
	DECLARE @currentElementId INT,
			@currentEntityId INT,
			@currentEntityName NVARCHAR(400)			

	---CURRENT ELEMENT FIELDS THAT ALREADY MIGRATED
	DECLARE @currentElementId_InNewDB INT,
			@currentEntityId_InNewDB INT			

	SELECT  @currentElementId = Id,
		    @currentEntityId = EntityId,
			@currentEntityName = EntityName
	FROM    @currentElement
	
	---MAIN SECTION	

	---GET Entity id
	SELECT @currentEntityId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentEntityId AND TableName = @currentEntityName
	IF (@currentEntityId_InNewDB IS NULL) AND (@currentEntityId IS NOT NULL)
		PRINT 'New Entity is not found'
	
	INSERT INTO [hpl2].dbo.UrlRecord
	        ( EntityId ,
	          EntityName ,
	          Slug ,
	          LanguageId ,
	          IsActive
	        )
	SELECT	@currentEntityId_InNewDB ,
	          EntityName ,
	          Slug ,
	          LanguageId ,
	          IsActive
	FROM @currentElement
	
	SET @currentElementId_InNewDB = SCOPE_IDENTITY();
		
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

PRINT N'MIGRATION FINISHED ['+ @TableName +']'