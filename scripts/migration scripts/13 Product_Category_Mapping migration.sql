USE spheres1
DECLARE @TableName varchar(100) = 'Product_Category_Mapping';  
DECLARE @CurrentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[ProductId] INT,
	[CategoryId] INT,
	[IsFeaturedProduct] BIT,
	[DisplayOrder] INT
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[ProductId] INT,
	[CategoryId] INT,
	[IsFeaturedProduct] BIT,
	[DisplayOrder] INT
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          ProductId ,
          CategoryId ,
          IsFeaturedProduct ,
          DisplayOrder
        )
SELECT Id ,
          ProductId ,
          CategoryId ,
          IsFeaturedProduct ,
          DisplayOrder
FROM [spheres1].dbo.Product_Category_Mapping

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentProductId INT,
		@currentCategoryId INT;

SELECT @currentElementId = Id,
	   @currentProductId = ProductId,
	   @currentCategoryId = CategoryId 
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
		    @currentProductIdInNewTable INT,
			@currentCategoryIdInNewTable INT;
	
	---GET PRODUCT
	SELECT @currentProductIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentProductId AND TableName = 'Product'
	IF (@currentProductIdInNewTable IS NULL) AND (@currentProductId IS NOT NULL)
		PRINT 'New Product is not found'

	---GET CATEGORY
	SELECT @currentCategoryIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentCategoryId AND TableName = 'Category'
	IF (@currentCategoryIdInNewTable IS NULL) AND (@currentCategoryId IS NOT NULL)
		PRINT 'New Category is not found'

	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.Product_Category_Mapping
	        ( ProductId ,
	          CategoryId ,
	          IsFeaturedProduct ,
	          DisplayOrder
	        )	
	SELECT  
			@currentProductIdInNewTable,
			@currentCategoryIdInNewTable,
			IsFeaturedProduct,
			DisplayOrder FROM @currentElement		
	---IMPORTANT
	
	--INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	--VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'