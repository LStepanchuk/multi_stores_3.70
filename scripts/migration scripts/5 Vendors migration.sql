Use [spheres1]
DECLARE @TableName varchar(20) = 'Vendor';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )

IF EXISTS (SELECT * FROM [spheres1].dbo.Vendor)
BEGIN

PRINT N'START TO MIGRATE ALL VENDORS'
BEGIN

DECLARE @AllVendors TABLE
(
	[Id] [INT],
	[Name] [NVARCHAR](400),
	[Email] [NVARCHAR](400),
	[Description] [NVARCHAR](MAX),
	[AdminComment] [NVARCHAR](MAX),
	[Active] [BIT],
	[Deleted] [BIT],
	[MetaKeywords] [NVARCHAR](400),
	[MetaDescription] [NVARCHAR](MAX),
	[MetaTitle] [NVARCHAR](400),
	[PageSize] [INT],
	[AllowCustomersToSelectPageSize] [BIT],
	[PageSizeOptions] [NVARCHAR](200),
	[DisplayOrder] [INT],
	[PictureId] [INT]
);

DECLARE @currentVendor TABLE
(
	[Id] [INT],
	[Name] [NVARCHAR](400),
	[Email] [NVARCHAR](400),
	[Description] [NVARCHAR](MAX),
	[AdminComment] [NVARCHAR](MAX),
	[Active] [BIT],
	[Deleted] [BIT],
	[MetaKeywords] [NVARCHAR](400),
	[MetaDescription] [NVARCHAR](MAX),
	[MetaTitle] [NVARCHAR](400),
	[PageSize] [INT],
	[AllowCustomersToSelectPageSize] [BIT],
	[PageSizeOptions] [NVARCHAR](200),
	[DisplayOrder] [INT],
	[PictureId] [INT]
);

INSERT INTO @AllVendors (Id, Name, Email, [Description], AdminComment, Active, Deleted, MetaKeywords, MetaDescription, MetaTitle, PageSize, AllowCustomersToSelectPageSize, PageSizeOptions, DisplayOrder, PictureId)
SELECT Id, Name, Email, [Description], AdminComment, Active, Deleted, MetaKeywords, MetaDescription, MetaTitle, PageSize, AllowCustomersToSelectPageSize, PageSizeOptions, DisplayOrder, PictureId 
FROM [spheres1].dbo.Vendor

PRINT N'START LOOP';
 
StartVendorsLoop:
PRINT N'Start new iteration';  

---GET FIRST ProductTemplate
DELETE FROM @currentVendor
INSERT INTO @currentVendor
SELECT TOP 1 * FROM @AllVendors

DECLARE @currentVendorId INT;
DECLARE @currentPictureId INT;
SELECT @currentVendorId = Id,
@currentPictureId = PictureId FROM @currentVendor

BEGIN
	DECLARE @vendorInNewTable INT;
	DECLARE @pictureFromNewTable INT;

	--SELECT @pictureFromNewTable = [NewId] FROM dbo.OldPicturesStorage
	--WHERE OldId = @currentPictureId;

	INSERT INTO [hpl2].dbo.Vendor
	        ( Name ,
	          Email ,
	          [Description] ,
	          AdminComment ,
	          Active ,
	          Deleted ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          PageSize ,
	          AllowCustomersToSelectPageSize ,
	          PageSizeOptions ,
	          DisplayOrder ,
	          PictureId
	        )	
	SELECT  Name ,
	          Email ,
	          [Description] ,
	          AdminComment ,
	          Active ,
	          Deleted ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          PageSize ,
	          AllowCustomersToSelectPageSize ,
	          PageSizeOptions ,
	          DisplayOrder ,
	          @pictureFromNewTable
	FROM @currentVendor
	
	SET @vendorInNewTable = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage (OldId, [NewId], TableName)
	VALUES (@currentVendorId, @vendorInNewTable, @TableName)

	DELETE FROM @AllVendors
	WHERE Id = @currentVendorId
END

IF EXISTS (SELECT * FROM @AllVendors)
	GOTO StartVendorsLoop

END
PRINT N'VENDORS MIGRATION FINISHED'


PRINT N'START TO MIGRATE ALL VENDORS NOTES'
BEGIN

DECLARE @AllVendorNotes TABLE
(
	[Id] INT,
	[VendorId] INT,
	[Note] [NVARCHAR](MAX),	
	[CreatedOnUtc] DATETIME
);

DECLARE @currentVendorNote TABLE
(
	[Id] INT,
	[VendorId] INT,
	[Note] [NVARCHAR](MAX),	
	[CreatedOnUtc] DATETIME
);

INSERT INTO @AllVendorNotes (Id, VendorId, Note, CreatedOnUtc)
SELECT Id, VendorId, Note, CreatedOnUtc 
FROM [spheres1].dbo.VendorNote

PRINT N'START LOOP';
 
StartVendorNotesLoop:
PRINT N'Start new iteration';  

DELETE FROM @currentVendorNote
INSERT INTO @currentVendorNote
SELECT TOP 1 * FROM @AllVendorNotes

DECLARE @currentVendorNoteId INT;
DECLARE @currentVendorNoteVendorId INT;
SELECT @currentVendorNoteId = Id,
@currentVendorNoteVendorId = VendorId FROM @currentVendorNote

BEGIN
	DECLARE @vendorNoteInNewTable INT;
	DECLARE @vendorFromNewStorage INT;

	SELECT @vendorFromNewStorage = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentVendorNoteVendorId AND TableName = @TableName;


	INSERT INTO [hpl2].dbo.VendorNote (VendorId, Note, CreatedOnUtc)	        
	SELECT @vendorFromNewStorage, Note, CreatedOnUtc
	FROM @currentVendorNote
	
	DELETE FROM @AllVendorNotes
	WHERE Id = @currentVendorNoteId
END

IF EXISTS (SELECT * FROM @AllVendorNotes)
	GOTO StartVendorNotesLoop

END
PRINT N'VENDORS NOTES MIGRATION FINISHED'

END