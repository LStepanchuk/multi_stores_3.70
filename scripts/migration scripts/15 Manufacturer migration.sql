USE [spheres1]
DECLARE @TableName varchar(100) = 'Manufacturer';  
DECLARE @CurrentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(400),
	[Description] NVARCHAR(MAX),
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[PictureId] INT, --PICTURES
	[PageSize] INT,
	[PriceRanges] NVARCHAR(400),
	[Published] BIT,
	[Deleted] BIT,
	[DisplayOrder] INT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[ManufacturerTemplateId] INT, --TEMPLATE
	[AllowCustomersToSelectPageSize] BIT,
	[PageSizeOptions] NVARCHAR(200),
	[SubjectToAcl] BIT,
	[LimitedToStores] BIT
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(400),
	[Description] NVARCHAR(MAX),
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[PictureId] INT, --PICTURES
	[PageSize] INT,
	[PriceRanges] NVARCHAR(400),
	[Published] BIT,
	[Deleted] BIT,
	[DisplayOrder] INT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[ManufacturerTemplateId] INT, --TEMPLATE
	[AllowCustomersToSelectPageSize] BIT,
	[PageSizeOptions] NVARCHAR(200),
	[SubjectToAcl] BIT,
	[LimitedToStores] BIT
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          Name ,
          [Description] ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          PictureId ,
          PageSize ,
          PriceRanges ,
          Published ,
          Deleted ,
          DisplayOrder ,
          CreatedOnUtc ,
          UpdatedOnUtc ,
          ManufacturerTemplateId ,
          AllowCustomersToSelectPageSize ,
          PageSizeOptions ,
          SubjectToAcl ,
          LimitedToStores
        )
SELECT Id ,
          Name ,
          [Description] ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          PictureId ,
          PageSize ,
          PriceRanges ,
          Published ,
          Deleted ,
          DisplayOrder ,
          CreatedOnUtc ,
          UpdatedOnUtc ,
          ManufacturerTemplateId ,
          AllowCustomersToSelectPageSize ,
          PageSizeOptions ,
          SubjectToAcl ,
          LimitedToStores
FROM [spheres1].dbo.Manufacturer

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);
DECLARE @currentPictureId INT;
SELECT @currentElementId = Id,
		@currentElementName = Name,
		@currentPictureId = PictureId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
			@pictureInNewTable INT;
	
	---GET PICTURE
	SELECT @pictureInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentPictureId AND TableName = 'Picture'
	IF (@pictureInNewTable IS NULL) AND (@currentPictureId IS NOT NULL)
		PRINT 'New Picture is not found'


	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.Manufacturer
	        ( Name ,
	          [Description] ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          PictureId ,
	          PageSize ,
	          PriceRanges ,
	          Published ,
	          Deleted ,
	          DisplayOrder ,
	          CreatedOnUtc ,
	          UpdatedOnUtc ,
	          ManufacturerTemplateId ,
	          AllowCustomersToSelectPageSize ,
	          PageSizeOptions ,
	          SubjectToAcl ,
	          LimitedToStores
	        )	
	SELECT  Name ,
	          [Description] ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          @pictureInNewTable ,
	          PageSize ,
	          PriceRanges ,
	          Published ,
	          Deleted ,
	          DisplayOrder ,
	          CreatedOnUtc ,
	          UpdatedOnUtc ,
	          ManufacturerTemplateId ,
	          AllowCustomersToSelectPageSize ,
	          PageSizeOptions ,
	          SubjectToAcl ,
	          1 FROM @currentElement
		
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	---UPDATE STORE MAPPING
	INSERT INTO [hpl2].dbo.StoreMapping ( EntityId, EntityName, StoreId )
	VALUES ( @currentElementIdInNewTable, @TableName, @CurrentStoreId )

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'