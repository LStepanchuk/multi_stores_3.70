USE [spheres1]
DECLARE @TableName varchar(100) = 'MessageTemplate';  
DECLARE @CurrentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(	
	[Id] [INT],
	[Name] [NVARCHAR](200),
	[BccEmailAddresses] [NVARCHAR](200),
	[Subject] [NVARCHAR](1000),
	[Body] [NVARCHAR](MAX),
	[IsActive] [BIT],
	[EmailAccountId] [INT],
	[LimitedToStores] [BIT],
	[AttachedDownloadId] [INT]
);

DECLARE @currentElement TABLE
(
	[Id] [INT],
	[Name] [NVARCHAR](200),
	[BccEmailAddresses] [NVARCHAR](200),
	[Subject] [NVARCHAR](1000),
	[Body] [NVARCHAR](MAX),
	[IsActive] [BIT],
	[EmailAccountId] [INT],
	[LimitedToStores] [BIT],
	[AttachedDownloadId] [INT]
);

INSERT INTO @AllElements
        ( Id ,
          Name ,
          BccEmailAddresses ,
          [Subject] ,
          Body ,
          IsActive ,
          EmailAccountId ,
          LimitedToStores ,
          AttachedDownloadId
        )
SELECT Id ,
          Name ,
          BccEmailAddresses ,
          [Subject] ,
          Body ,
          IsActive ,
          EmailAccountId ,
          LimitedToStores ,
          AttachedDownloadId
FROM [spheres1].dbo.MessageTemplate

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentEmailId INT;

SELECT @currentElementId = Id,
	   @currentEmailId = EmailAccountId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
			@currentEmailInNewTable INT

	---GET EMAIL
	SELECT @currentEmailInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentEmailId AND TableName = 'EmailAccount'
	IF (@currentEmailInNewTable IS NULL) AND (@currentEmailId IS NOT NULL)
		PRINT 'New EmailAccount is not found'		

	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.MessageTemplate
	        ( Name ,
	          BccEmailAddresses ,
	          [Subject] ,
	          Body ,
	          IsActive ,
	          EmailAccountId ,
	          LimitedToStores ,
	          AttachedDownloadId
	        )	
	SELECT  Name ,
	          BccEmailAddresses ,
	          [Subject] ,
	          Body ,
	          IsActive ,
	          @currentEmailInNewTable ,
	          1 ,
	          AttachedDownloadId FROM @currentElement
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	---UPDATE STORE MAPPING
	INSERT INTO [hpl2].dbo.StoreMapping ( EntityId, EntityName, StoreId )
	VALUES ( @currentElementIdInNewTable, @TableName, @CurrentStoreId )

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'