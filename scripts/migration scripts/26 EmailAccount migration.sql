USE [spheres1]
DECLARE @TableName varchar(100) = 'EmailAccount';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage (OldId, [NewId], TableName) VALUES (0, 0, @TableName)

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(	
	[Id] [int],
	[Email] [nvarchar](255),
	[DisplayName] [nvarchar](255),
	[Host] [nvarchar](255),
	[Port] [int],
	[Username] [nvarchar](255),
	[Password] [nvarchar](255),
	[EnableSsl] [bit],
	[UseDefaultCredentials] bit
);

DECLARE @currentElement TABLE
(
	[Id] [int],
	[Email] [nvarchar](255),
	[DisplayName] [nvarchar](255),
	[Host] [nvarchar](255),
	[Port] [int],
	[Username] [nvarchar](255),
	[Password] [nvarchar](255),
	[EnableSsl] [bit],
	[UseDefaultCredentials] bit
);

INSERT INTO @AllElements
        ( Id ,
          Email ,
          DisplayName ,
          Host ,
          Port ,
          Username ,
          [Password] ,
          EnableSsl ,
          UseDefaultCredentials
        )
SELECT Id ,
          Email ,
          DisplayName ,
          Host ,
          Port ,
          Username ,
          [Password] ,
          EnableSsl ,
          UseDefaultCredentials 
FROM [spheres1].dbo.EmailAccount

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;

SELECT @currentElementId = Id
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.EmailAccount
	        ( Email ,
	          DisplayName ,
	          Host ,
	          Port ,
	          Username ,
	          [Password] ,
	          EnableSsl ,
	          UseDefaultCredentials
	        )	
	SELECT  Email ,
	        DisplayName ,
	        Host ,
	        Port ,
	        Username ,
	        [Password] ,
	        EnableSsl ,
	        UseDefaultCredentials FROM @currentElement
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'