USE spheres1
DECLARE @TableName varchar(100) = 'Customer';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	[Id] INT,
	[CustomerGuid] UNIQUEIDENTIFIER,
	[Username] NVARCHAR(1000),
	[Email] NVARCHAR(1000),
	[Password] NVARCHAR(MAX),
	[PasswordFormatId] INT,
	[PasswordSalt] NVARCHAR(MAX),
	[AdminComment] NVARCHAR(MAX),
	[IsTaxExempt] BIT,
	[AffiliateId] INT, --Affiliate
	[Active] BIT,
	[Deleted] BIT,
	[IsSystemAccount] BIT,
	[SystemName] NVARCHAR(400),
	[LastIpAddress] [NVARCHAR](MAX),
	[CreatedOnUtc] DATETIME,
	[LastLoginDateUtc] DATETIME,
	[LastActivityDateUtc] DATETIME,
	[BillingAddress_Id] INT, --Address
	[ShippingAddress_Id] INT, --Address
	[VendorId] INT, --Vendor
	[HasShoppingCartItems] BIT
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[CustomerGuid] UNIQUEIDENTIFIER,
	[Username] NVARCHAR(1000),
	[Email] NVARCHAR(1000),
	[Password] NVARCHAR(MAX),
	[PasswordFormatId] INT,
	[PasswordSalt] NVARCHAR(MAX),
	[AdminComment] NVARCHAR(MAX),
	[IsTaxExempt] BIT,
	[AffiliateId] INT, --Affiliate
	[Active] BIT,
	[Deleted] BIT,
	[IsSystemAccount] BIT,
	[SystemName] NVARCHAR(400),
	[LastIpAddress] [NVARCHAR](MAX),
	[CreatedOnUtc] DATETIME,
	[LastLoginDateUtc] DATETIME,
	[LastActivityDateUtc] DATETIME,
	[BillingAddress_Id] INT, --Address
	[ShippingAddress_Id] INT, --Address
	[VendorId] INT, --Vendor
	[HasShoppingCartItems] BIT
);

INSERT INTO @AllElements
        ( Id ,
          CustomerGuid ,
          Username ,
          Email ,
          [Password] ,
          PasswordFormatId ,
          PasswordSalt ,
          AdminComment ,
          IsTaxExempt ,
          AffiliateId ,
          Active ,
          Deleted ,
          IsSystemAccount ,
          SystemName ,
          LastIpAddress ,
          CreatedOnUtc ,
          LastLoginDateUtc ,
          LastActivityDateUtc ,
          BillingAddress_Id ,
          ShippingAddress_Id ,
          VendorId ,
          HasShoppingCartItems
        )
SELECT Id ,
          CustomerGuid ,
          Username ,
          Email ,
          [Password] ,
          PasswordFormatId ,
          PasswordSalt ,
          AdminComment ,
          IsTaxExempt ,
          AffiliateId ,
          Active ,
          Deleted ,
          IsSystemAccount ,
          SystemName ,
          LastIpAddress ,
          CreatedOnUtc ,
          LastLoginDateUtc ,
          LastActivityDateUtc ,
          BillingAddress_Id ,
          ShippingAddress_Id ,
          VendorId ,
          HasShoppingCartItems 
FROM [spheres1].dbo.Customer

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentEmail NVARCHAR(1000),
		@currentAffiliateId INT,
		@currentBillingAddress_Id INT,
        @currentShippingAddress_Id INT, 
		@currentVendorId INT

SELECT  @currentElementId = Id,
		@currentEmail = Email,
		@currentAffiliateId = AffiliateId,
		@currentBillingAddress_Id = BillingAddress_Id,
        @currentShippingAddress_Id = ShippingAddress_Id, 
		@currentVendorId = VendorId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
			@currentAffiliateIdInNewTable INT,
			@currentBillingAddress_IdInNewTable INT,
			@currentShippingAddress_IdInNewTable INT, 
			@currentVendorIdInNewTable INT
			
	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.Customer WHERE Email = @currentEmail)
	BEGIN
		SELECT @currentElementIdInNewTable = Id 
		FROM [hpl2].dbo.Customer 
		WHERE Email = @currentEmail
	END
	ELSE
	BEGIN
		---GET AFFILIATE
		SET @currentAffiliateIdInNewTable = 0;

		---GET BILLING ADDRESS
		SELECT @currentBillingAddress_IdInNewTable = [NewId] FROM dbo.OldElementsStorage
		WHERE OldId = @currentBillingAddress_Id AND TableName = 'Address'

		---GET SHIPPING ADDRESS
		SELECT @currentShippingAddress_IdInNewTable = [NewId] FROM dbo.OldElementsStorage
		WHERE OldId = @currentAffiliateId AND TableName = 'Address'

		---GET VENDOR
		SELECT @currentVendorIdInNewTable = [NewId] FROM dbo.OldElementsStorage
		WHERE OldId = @currentVendorId AND TableName = 'Vendor'

		INSERT INTO [hpl2].dbo.Customer
		        ( CustomerGuid ,
		          Username ,
		          Email ,
		          [Password] ,
		          PasswordFormatId ,
		          PasswordSalt ,
		          AdminComment ,
		          IsTaxExempt ,
		          AffiliateId ,
		          Active ,
		          Deleted ,
		          IsSystemAccount ,
		          SystemName ,
		          LastIpAddress ,
		          CreatedOnUtc ,
		          LastLoginDateUtc ,
		          LastActivityDateUtc ,
		          BillingAddress_Id ,
		          ShippingAddress_Id ,
		          VendorId ,
		          HasShoppingCartItems
		        )		       
		SELECT  CustomerGuid ,
		        Username ,
		        Email ,
		        [Password] ,
		        PasswordFormatId ,
		        PasswordSalt ,
		        AdminComment ,
		        IsTaxExempt ,
		        @currentAffiliateIdInNewTable ,
		        Active ,
		        Deleted ,
		        IsSystemAccount ,
		        SystemName ,
		        LastIpAddress ,
		        CreatedOnUtc ,
		        LastLoginDateUtc ,
		        LastActivityDateUtc ,
		        @currentBillingAddress_IdInNewTable ,
		        @currentShippingAddress_IdInNewTable ,
		        @currentVendorIdInNewTable ,
		        HasShoppingCartItems FROM @currentElement
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END		
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'
