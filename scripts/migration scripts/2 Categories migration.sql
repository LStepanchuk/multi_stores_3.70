
DECLARE @TableName varchar(20) = 'Category';  
DECLARE @currentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

DECLARE @currentEntityName NVARCHAR(400);
SET @currentEntityName = 'Category';

DECLARE @ALLCategories TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[Description] NVARCHAR(MAX),
	[CategoryTemplateId] INT,
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[ParentCategoryId] INT,
	[PictureId] INT,
	[PageSize] INT,
	[AllowCustomersToSelectPageSize] BIT,
	[PageSizeOptions] NVARCHAR(200),
	[PriceRanges] NVARCHAR(400),
	[ShowOnHomePage] BIT,
	[Published] BIT,
	[Deleted] BIT,
	[DisplayOrder] INT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[SubjectToAcl] BIT ,
	[LimitedToStores] BIT,
	[IncludeInTopMenu] BIT
);		

DECLARE @currentCategory TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[Description] NVARCHAR(MAX),
	[CategoryTemplateId] INT,
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[ParentCategoryId] INT,
	[PictureId] [INT],
	[PageSize] [INT],
	[AllowCustomersToSelectPageSize] BIT,
	[PageSizeOptions] NVARCHAR(200),
	[PriceRanges] NVARCHAR(400),
	[ShowOnHomePage] BIT,
	[Published] BIT,
	[Deleted] BIT,
	[DisplayOrder] INT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[SubjectToAcl] BIT ,
	[LimitedToStores] BIT,
	[IncludeInTopMenu] BIT
);	

---SAVE ALL CATEGORIES IN TEMP TABLE
INSERT INTO @ALLCategories
SELECT * FROM [spheres1].dbo.Category

PRINT N'START LOOP'; 
StartLoop:
PRINT N'Start new iteration';  

---GET FIRST CATEGORY
DELETE FROM @currentCategory
INSERT INTO @currentCategory
SELECT TOP 1 * FROM @ALLCategories

DECLARE @currentCategoryId INT;
DECLARE @currentParentId INT;
DECLARE @currentTemplateId INT;

SELECT @currentCategoryId = Id,
	   @currentParentId = ParentCategoryId,
	   @currentTemplateId = CategoryTemplateId
FROM @currentCategory

ForParentCategory:

PRINT 'PROCESS CATEGORY: ' + CAST(@currentCategoryId AS nvarchar(30));

BEGIN
	DECLARE @templateCategoryIdInNewTable INT;
	DECLARE @templateCategoryIdInOldTable INT;
	DECLARE @parentCategoryInNewTable INT;
	DECLARE @currentCategoryInNewTable INT;

	DECLARE @categoryTemplateForInsert TABLE
	(
		[Id] INT,
		[Name] NVARCHAR(400),
		[ViewPath] NVARCHAR(400),
		[DisplayOrder] INT
	);
	
	---CHECK IF EXIST PARENT CATEGORY	
	IF @currentParentId != 0
	BEGIN
		IF EXISTS (SELECT * FROM OldElementsStorage WHERE OldId = @currentParentId AND TableName = @TableName)
		BEGIN
			SELECT @parentCategoryInNewTable = [NewId] FROM OldElementsStorage
			WHERE OldId = @currentParentId
		END
		ELSE
		BEGIN
			---GET ANOTHER CATEGORY
			DELETE FROM @currentCategory
			INSERT INTO @currentCategory
			SELECT TOP 1 * FROM @ALLCategories
			WHERE Id = @currentParentId
			SELECT @currentCategoryId = Id,
				   @currentParentId = ParentCategoryId,
	               @currentTemplateId = CategoryTemplateId
			FROM @currentCategory

			GOTO ForParentCategory
		END		
	END
	ELSE
	BEGIN
		SET @parentCategoryInNewTable = 0;
	END
	
	DECLARE @currentCategoryName NVARCHAR(400);
	SELECT @currentCategoryName = ViewPath FROM [spheres1].dbo.CategoryTemplate WHERE Id = @currentTemplateId

	---ADD CATEGORY TEMPLATE IF NOT EXIST
	IF EXISTS (SELECT * FROM [hpl2].dbo.CategoryTemplate WHERE ViewPath = @currentCategoryName)
		SELECT @templateCategoryIdInNewTable = Id FROM [hpl2].dbo.CategoryTemplate WHERE ViewPath = @currentCategoryName
	ELSE
	BEGIN
		DELETE FROM @categoryTemplateForInsert
		INSERT INTO @categoryTemplateForInsert
		SELECT TOP 1 * FROM [spheres1].dbo.CategoryTemplate
		WHERE Id = @currentTemplateId
		
		---INSERT NEW CATEGORY TEMPLATE	
		INSERT INTO [hpl2].dbo.CategoryTemplate ( Name, ViewPath, DisplayOrder )
		SELECT TOP 1 Name, ViewPath, DisplayOrder FROM @categoryTemplateForInsert

		SET @templateCategoryIdInNewTable = SCOPE_IDENTITY();
	END

	---INSERT NEW CATEGORY
	INSERT INTO [hpl2].dbo.Category
	        ( [Name], 
			  [Description],
	          [CategoryTemplateId],
	          [MetaKeywords],
	          [MetaDescription],
	          [MetaTitle],
	          [ParentCategoryId],
	          [PictureId],
	          [PageSize],
	          [AllowCustomersToSelectPageSize],
	          [PageSizeOptions],
	          [PriceRanges],
	          [ShowOnHomePage],
	          [Published],
	          [Deleted],
	          [DisplayOrder],
	          [CreatedOnUtc],
	          [UpdatedOnUtc],
	          [SubjectToAcl],
	          [LimitedToStores],
	          [IncludeInTopMenu])
	SELECT    [Name], 
			  [Description],
	          @templateCategoryIdInNewTable,
	          [MetaKeywords],
	          [MetaDescription],
	          [MetaTitle],
	          @parentCategoryInNewTable,
	          [PictureId],
	          [PageSize],
	          [AllowCustomersToSelectPageSize],
	          [PageSizeOptions],
	          [PriceRanges],
	          [ShowOnHomePage],
	          [Published],
	          [Deleted],
	          [DisplayOrder],
	          [CreatedOnUtc],
	          [UpdatedOnUtc],
	          [SubjectToAcl],
	          1,
	          [IncludeInTopMenu]
	FROM @currentCategory

	SET @currentCategoryInNewTable = SCOPE_IDENTITY();

	---SAVE RELATIONS BEETWEEN OLD AND NEW ID
	INSERT INTO dbo.OldElementsStorage ( [OldId], [NewId], [TableName] )
	VALUES (@currentCategoryId, @currentCategoryInNewTable, @TableName);
	
	---DELETE ROW FROM TEMP TABLE
	DELETE FROM @ALLCategories
	WHERE Id = @currentCategoryId

	PRINT 'CATEGORY [' +  CAST(@currentCategoryId AS nvarchar(30)) + '] SAVED'
END   

IF EXISTS (SELECT * FROM @ALLCategories)
	GOTO StartLoop
ELSE
	GOTO EndLoop

PRINT 'END OF LOOP'

EndLoop:


---UPDATE STORE MAPPING
INSERT INTO [hpl2].dbo.StoreMapping ( EntityId, EntityName, StoreId )
SELECT [NewId], @currentEntityName, @currentStoreId FROM dbo.OldElementsStorage
WHERE TableName = @TableName

PRINT 'STORE MAPPING UPDATED'