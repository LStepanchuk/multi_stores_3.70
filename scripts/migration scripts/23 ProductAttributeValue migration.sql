USE spheres1
DECLARE @TableName varchar(100) = 'ProductAttributeValue';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[ProductAttributeMappingId] [INT],
	[Name] [NVARCHAR](400),
	[PriceAdjustment] [DECIMAL](18, 4),
	[WeightAdjustment] [DECIMAL](18, 4),
	[IsPreSelected] [BIT],
	[DisplayOrder] [INT],
	[ColorSquaresRgb] [NVARCHAR](100),
	[PictureId] [INT],
	[AttributeValueTypeId] [INT],
	[AssociatedProductId] [INT],
	[Cost] [DECIMAL](18, 4),
	[Quantity] [INT]
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] [INT],
	[ProductAttributeMappingId] [INT],
	[Name] [NVARCHAR](400),
	[PriceAdjustment] [DECIMAL](18, 4),
	[WeightAdjustment] [DECIMAL](18, 4),
	[IsPreSelected] [BIT],
	[DisplayOrder] [INT],
	[ColorSquaresRgb] [NVARCHAR](100),
	[PictureId] [INT],
	[AttributeValueTypeId] [INT],
	[AssociatedProductId] [INT],
	[Cost] [DECIMAL](18, 4),
	[Quantity] [INT]
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          ProductAttributeMappingId ,
          Name ,
          PriceAdjustment ,
          WeightAdjustment ,
          IsPreSelected ,
          DisplayOrder ,
          ColorSquaresRgb ,
          PictureId ,
          AttributeValueTypeId ,
          AssociatedProductId ,
          Cost ,
          Quantity
        )
SELECT Id ,
          ProductAttributeMappingId ,
          Name ,
          PriceAdjustment ,
          WeightAdjustment ,
          IsPreSelected ,
          DisplayOrder ,
          ColorSquaresRgb ,
          PictureId ,
          AttributeValueTypeId ,
          AssociatedProductId ,
          Cost ,
          Quantity
FROM [spheres1].dbo.ProductAttributeValue

PRINT N'START LOOP';
WHILE EXISTS (SELECT * FROM @AllElements)
BEGIN
	---GET FIRST ELEMENT
	DELETE FROM @currentElement
	INSERT INTO @currentElement
	SELECT TOP 1 * FROM @AllElements

	---CURRENT ELEMENT FIELDS
	DECLARE @currentElementId INT,
			@currentProductAttributeMappingId INT,
			@currentPictureId INT;

	---CURRENT ELEMENT FIELDS THAT ALREADY MIGRATED
	DECLARE @currentElementId_InNewDB INT,
			@currentProductAttributeMappingId_InNewDB INT,
			@currentPictureId_InNewDB INT;

	SELECT  @currentElementId = Id,
		    @currentProductAttributeMappingId = ProductAttributeMappingId,
			@currentPictureId = PictureId
	FROM    @currentElement
	
	---MAIN SECTION	

	---GET PRODUCT ATTRIBUTE MAPPING
	SELECT @currentProductAttributeMappingId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentProductAttributeMappingId AND TableName = 'Product_ProductAttribute_Mapping'
	IF (@currentProductAttributeMappingId_InNewDB IS NULL) AND (@currentProductAttributeMappingId IS NOT NULL)
		PRINT 'New Product_ProductAttribute_Mapping is not found'

	---GET PICTURE
	SELECT @currentPictureId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentPictureId AND TableName = 'Picture'
	IF (@currentPictureId_InNewDB IS NULL) AND (@currentPictureId IS NOT NULL)
		PRINT 'New Picture is not found'

	INSERT INTO [hpl2].dbo.ProductAttributeValue
	        ( ProductAttributeMappingId ,
	          Name ,
	          PriceAdjustment ,
	          WeightAdjustment ,
	          IsPreSelected ,
	          DisplayOrder ,
	          ColorSquaresRgb ,
	          PictureId ,
	          AttributeValueTypeId ,
	          AssociatedProductId ,
	          Cost ,
	          Quantity
	        )	
	SELECT	@currentProductAttributeMappingId_InNewDB ,
	          Name ,
	          PriceAdjustment ,
	          WeightAdjustment ,
	          IsPreSelected ,
	          DisplayOrder ,
	          ColorSquaresRgb ,
	          @currentPictureId_InNewDB ,
	          AttributeValueTypeId ,
	          AssociatedProductId ,
	          Cost ,
	          Quantity
	FROM @currentElement
	
	SET @currentElementId_InNewDB = SCOPE_IDENTITY();
		
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

PRINT N'MIGRATION FINISHED ['+ @TableName +']'