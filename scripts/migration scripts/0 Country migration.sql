USE [spheres1]
DECLARE @TableName varchar(100) = 'Country';  

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(	
	[Id] INT,
	[Name] NVARCHAR(100),
	[AllowsBilling] BIT,
	[AllowsShipping] BIT,
	[TwoLetterIsoCode] NVARCHAR(2),
	[ThreeLetterIsoCode] NVARCHAR(3),
	[NumericIsoCode] INT,
	[SubjectToVat] BIT,
	[Published] BIT,
	[DisplayOrder] INT,
	[LimitedToStores] BIT	
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[Name] NVARCHAR(100),
	[AllowsBilling] BIT,
	[AllowsShipping] BIT,
	[TwoLetterIsoCode] NVARCHAR(2),
	[ThreeLetterIsoCode] NVARCHAR(3),
	[NumericIsoCode] INT,
	[SubjectToVat] BIT,
	[Published] BIT,
	[DisplayOrder] INT,
	[LimitedToStores] BIT
);

INSERT INTO @AllElements
        ( Id, Name, AllowsBilling, AllowsShipping, TwoLetterIsoCode, ThreeLetterIsoCode, NumericIsoCode, SubjectToVat, Published, DisplayOrder, LimitedToStores)
SELECT Id, Name, AllowsBilling, AllowsShipping, TwoLetterIsoCode, ThreeLetterIsoCode, NumericIsoCode, SubjectToVat, Published, DisplayOrder, LimitedToStores 
FROM [spheres1].dbo.Country

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);

SELECT @currentElementId = Id,
	   @currentElementName = Name
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---INSERT NEW ELEMENT INTO TARGET TABLE
	IF EXISTS(SELECT * FROM [hpl2].dbo.Country WHERE Name = @currentElementName)
	BEGIN
		SELECT @currentElementIdInNewTable = Id 
		FROM [hpl2].dbo.Country 
		WHERE Name = @currentElementName
	END
	ELSE
	BEGIN
		INSERT INTO [hpl2].dbo.Country
	        ( Name, AllowsBilling, AllowsShipping, TwoLetterIsoCode, ThreeLetterIsoCode, NumericIsoCode, SubjectToVat, Published, DisplayOrder, LimitedToStores )	
		SELECT  Name, AllowsBilling, AllowsShipping, TwoLetterIsoCode, ThreeLetterIsoCode, NumericIsoCode, SubjectToVat, Published, DisplayOrder, LimitedToStores FROM @currentElement
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END		

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)
	
	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'