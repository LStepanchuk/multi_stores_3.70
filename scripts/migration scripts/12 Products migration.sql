USE spheres1
DECLARE @TableName varchar(100) = 'Product';  
DECLARE @CurrentStoreId INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
  
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )
PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(400),
	[ShortDescription] NVARCHAR(MAX),
	[FullDescription] NVARCHAR(MAX),
	[AdminComment] NVARCHAR(MAX),
	[ShowOnHomePage] BIT,
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[AllowCustomerReviews] BIT,
	[ApprovedRatingSum] INT,
	[NotApprovedRatingSum] INT,
	[ApprovedTotalReviews] INT,
	[NotApprovedTotalReviews] INT,
	[Published] BIT,
	[Deleted] BIT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[ProductTemplateId] INT, --save template
	[SubjectToAcl] BIT,
	[LimitedToStores] BIT,
	[VendorId] INT, --save vendor
	[ProductTypeId] INT, --save product type
	[ParentGroupedProductId] INT, --0
	[SKU] NVARCHAR(400),
	[ManufacturerPartNumber] NVARCHAR(400),
	[Gtin] NVARCHAR(400),
	[IsGiftCard] BIT,
	[GiftCardTypeId] INT, --0
	[RequireOtherProducts] BIT,
	[RequiredProductIds] NVARCHAR(1000),
	[AutomaticallyAddRequiredProducts] BIT,
	[IsDownload] BIT,
	[DownloadId] INT, --copy downloads first, but always 0
	[UnlimitedDownloads] BIT,
	[MaxNumberOfDownloads] INT,
	[DownloadExpirationDays] INT,
	[DownloadActivationTypeId] INT,--1
	[HasSampleDownload] BIT,
	[SampleDownloadId] INT, --downloads
	[HasUserAgreement] BIT,
	[UserAgreementText] NVARCHAR(MAX),
	[IsRecurring] BIT,
	[RecurringCycleLength] INT,
	[RecurringCyclePeriodId] INT, --0
	[RecurringTotalCycles] INT,
	[IsShipEnabled] BIT,
	[IsFreeShipping] BIT,
	[AdditionalShippingCharge] DECIMAL(18, 4),
	[IsTaxExempt] BIT,
	[TaxCategoryId] INT, --TaxCategory (migrate first)
	[ManageInventoryMethodId] INT,
	[StockQuantity] INT,
	[DisplayStockAvailability] BIT,
	[DisplayStockQuantity] BIT,
	[MinStockQuantity] INT,
	[LowStockActivityId] INT,
	[NotifyAdminForQuantityBelow] INT,
	[BackorderModeId] INT,
	[AllowBackInStockSubscriptions] BIT,
	[OrderMinimumQuantity] INT,
	[OrderMaximumQuantity] INT,
	[AllowedQuantities] NVARCHAR(1000),
	[DisableBuyButton] BIT,
	[DisableWishlistButton] BIT,
	[AvailableForPreOrder] BIT,
	[CallForPrice] BIT,
	[Price] DECIMAL(18, 4),
	[OldPrice] DECIMAL(18, 4),
	[ProductCost] DECIMAL(18, 4),
	[SpecialPrice] DECIMAL(18, 4),
	[SpecialPriceStartDateTimeUtc] DATETIME,
	[SpecialPriceEndDateTimeUtc] DATETIME,
	[CustomerEntersPrice] BIT,
	[MinimumCustomerEnteredPrice] DECIMAL(18, 4),
	[MaximumCustomerEnteredPrice] DECIMAL(18, 4),
	[HasTierPrices] BIT,
	[HasDiscountsApplied] BIT,
	[Weight] DECIMAL(18, 4),
	[Length] DECIMAL(18, 4),
	[Width] DECIMAL(18, 4),
	[Height] DECIMAL(18, 4),
	[AvailableStartDateTimeUtc] DATETIME,
	[AvailableEndDateTimeUtc] DATETIME,
	[VisibleIndividually] BIT,
	[DisplayOrder] INT,
	[PreOrderAvailabilityStartDateTimeUtc] DATETIME,
	[DeliveryDateId] INT, --copy delivery Dates
	[WarehouseId] INT, --copy warehouseId
	[AllowAddingOnlyExistingAttributeCombinations] BIT,
	[ShipSeparately] BIT,
	[UseMultipleWarehouses] BIT,
	[IsRental] BIT,
	[RentalPriceLength] INT,
	[RentalPricePeriodId] INT,
	[IsTelecommunicationsOrBroadcastingOrElectronicServices] BIT,
	[BasepriceEnabled] BIT,
	[BasepriceAmount] DECIMAL(18, 4),
	[BasepriceUnitId] INT,
	[BasepriceBaseAmount] DECIMAL(18, 4),
	[BasepriceBaseUnitId] INT,
	[OverriddenGiftCardAmount] DECIMAL(18, 0),
	[MarkAsNew] BIT,
	[MarkAsNewStartDateTimeUtc] DATETIME,
	[MarkAsNewEndDateTimeUtc] DATETIME
	---IMPORTANT
);

DECLARE @currentElement TABLE
(
	---IMPORTANT---MUST BE EDITED
	[Id] INT,
	[Name] NVARCHAR(400),
	[ShortDescription] NVARCHAR(MAX),
	[FullDescription] NVARCHAR(MAX),
	[AdminComment] NVARCHAR(MAX),
	[ShowOnHomePage] BIT,
	[MetaKeywords] NVARCHAR(400),
	[MetaDescription] NVARCHAR(MAX),
	[MetaTitle] NVARCHAR(400),
	[AllowCustomerReviews] BIT,
	[ApprovedRatingSum] INT,
	[NotApprovedRatingSum] INT,
	[ApprovedTotalReviews] INT,
	[NotApprovedTotalReviews] INT,
	[Published] BIT,
	[Deleted] BIT,
	[CreatedOnUtc] DATETIME,
	[UpdatedOnUtc] DATETIME,
	[ProductTemplateId] INT, --save template
	[SubjectToAcl] BIT,
	[LimitedToStores] BIT,
	[VendorId] INT, --save vendor
	[ProductTypeId] INT, --save product type
	[ParentGroupedProductId] INT, --0
	[SKU] NVARCHAR(400),
	[ManufacturerPartNumber] NVARCHAR(400),
	[Gtin] NVARCHAR(400),
	[IsGiftCard] BIT,
	[GiftCardTypeId] INT, --0
	[RequireOtherProducts] BIT,
	[RequiredProductIds] NVARCHAR(1000),
	[AutomaticallyAddRequiredProducts] BIT,
	[IsDownload] BIT,
	[DownloadId] INT, --copy downloads first, but always 0
	[UnlimitedDownloads] BIT,
	[MaxNumberOfDownloads] INT,
	[DownloadExpirationDays] INT,
	[DownloadActivationTypeId] INT,--1
	[HasSampleDownload] BIT,
	[SampleDownloadId] INT, --downloads
	[HasUserAgreement] BIT,
	[UserAgreementText] NVARCHAR(MAX),
	[IsRecurring] BIT,
	[RecurringCycleLength] INT,
	[RecurringCyclePeriodId] INT, --0
	[RecurringTotalCycles] INT,
	[IsShipEnabled] BIT,
	[IsFreeShipping] BIT,
	[AdditionalShippingCharge] DECIMAL(18, 4),
	[IsTaxExempt] BIT,
	[TaxCategoryId] INT, --TaxCategory (migrate first)
	[ManageInventoryMethodId] INT,
	[StockQuantity] INT,
	[DisplayStockAvailability] BIT,
	[DisplayStockQuantity] BIT,
	[MinStockQuantity] INT,
	[LowStockActivityId] INT,
	[NotifyAdminForQuantityBelow] INT,
	[BackorderModeId] INT,
	[AllowBackInStockSubscriptions] BIT,
	[OrderMinimumQuantity] INT,
	[OrderMaximumQuantity] INT,
	[AllowedQuantities] NVARCHAR(1000),
	[DisableBuyButton] BIT,
	[DisableWishlistButton] BIT,
	[AvailableForPreOrder] BIT,
	[CallForPrice] BIT,
	[Price] DECIMAL(18, 4),
	[OldPrice] DECIMAL(18, 4),
	[ProductCost] DECIMAL(18, 4),
	[SpecialPrice] DECIMAL(18, 4),
	[SpecialPriceStartDateTimeUtc] DATETIME,
	[SpecialPriceEndDateTimeUtc] DATETIME,
	[CustomerEntersPrice] BIT,
	[MinimumCustomerEnteredPrice] DECIMAL(18, 4),
	[MaximumCustomerEnteredPrice] DECIMAL(18, 4),
	[HasTierPrices] BIT,
	[HasDiscountsApplied] BIT,
	[Weight] DECIMAL(18, 4),
	[Length] DECIMAL(18, 4),
	[Width] DECIMAL(18, 4),
	[Height] DECIMAL(18, 4),
	[AvailableStartDateTimeUtc] DATETIME,
	[AvailableEndDateTimeUtc] DATETIME,
	[VisibleIndividually] BIT,
	[DisplayOrder] INT,
	[PreOrderAvailabilityStartDateTimeUtc] DATETIME,
	[DeliveryDateId] INT, --copy
	[WarehouseId] INT, --copy
	[AllowAddingOnlyExistingAttributeCombinations] BIT,
	[ShipSeparately] BIT,
	[UseMultipleWarehouses] BIT,
	[IsRental] BIT,
	[RentalPriceLength] INT,
	[RentalPricePeriodId] INT,
	[IsTelecommunicationsOrBroadcastingOrElectronicServices] BIT,
	[BasepriceEnabled] BIT,
	[BasepriceAmount] DECIMAL(18, 4),
	[BasepriceUnitId] INT, --0
	[BasepriceBaseAmount] DECIMAL(18, 4),
	[BasepriceBaseUnitId] INT, --0
	[OverriddenGiftCardAmount] DECIMAL(18, 0),
	[MarkAsNew] BIT,
	[MarkAsNewStartDateTimeUtc] DATETIME,
	[MarkAsNewEndDateTimeUtc] DATETIME
	---IMPORTANT
);

INSERT INTO @AllElements
        ( Id ,
          Name ,
          ShortDescription ,
          FullDescription ,
          AdminComment ,
          ShowOnHomePage ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          AllowCustomerReviews ,
          ApprovedRatingSum ,
          NotApprovedRatingSum ,
          ApprovedTotalReviews ,
          NotApprovedTotalReviews ,
          Published ,
          Deleted ,
          CreatedOnUtc ,
          UpdatedOnUtc ,
          ProductTemplateId ,
          SubjectToAcl ,
          LimitedToStores ,
          VendorId ,
          ProductTypeId ,
          ParentGroupedProductId ,
          SKU ,
          ManufacturerPartNumber ,
          Gtin ,
          IsGiftCard ,
          GiftCardTypeId ,
          RequireOtherProducts ,
          RequiredProductIds ,
          AutomaticallyAddRequiredProducts ,
          IsDownload ,
          DownloadId ,
          UnlimitedDownloads ,
          MaxNumberOfDownloads ,
          DownloadExpirationDays ,
          DownloadActivationTypeId ,
          HasSampleDownload ,
          SampleDownloadId ,
          HasUserAgreement ,
          UserAgreementText ,
          IsRecurring ,
          RecurringCycleLength ,
          RecurringCyclePeriodId ,
          RecurringTotalCycles ,
          IsShipEnabled ,
          IsFreeShipping ,
          AdditionalShippingCharge ,
          IsTaxExempt ,
          TaxCategoryId ,
          ManageInventoryMethodId ,
          StockQuantity ,
          DisplayStockAvailability ,
          DisplayStockQuantity ,
          MinStockQuantity ,
          LowStockActivityId ,
          NotifyAdminForQuantityBelow ,
          BackorderModeId ,
          AllowBackInStockSubscriptions ,
          OrderMinimumQuantity ,
          OrderMaximumQuantity ,
          AllowedQuantities ,
          DisableBuyButton ,
          DisableWishlistButton ,
          AvailableForPreOrder ,
          CallForPrice ,
          Price ,
          OldPrice ,
          ProductCost ,
          SpecialPrice ,
          SpecialPriceStartDateTimeUtc ,
          SpecialPriceEndDateTimeUtc ,
          CustomerEntersPrice ,
          MinimumCustomerEnteredPrice ,
          MaximumCustomerEnteredPrice ,
          HasTierPrices ,
          HasDiscountsApplied ,
          [Weight] ,
          [Length] ,
          Width ,
          Height ,
          AvailableStartDateTimeUtc ,
          AvailableEndDateTimeUtc ,
          VisibleIndividually ,
          DisplayOrder ,
          PreOrderAvailabilityStartDateTimeUtc ,
          DeliveryDateId ,
          WarehouseId ,
          AllowAddingOnlyExistingAttributeCombinations ,
          ShipSeparately ,
          UseMultipleWarehouses ,
          IsRental ,
          RentalPriceLength ,
          RentalPricePeriodId ,
          IsTelecommunicationsOrBroadcastingOrElectronicServices ,
          BasepriceEnabled ,
          BasepriceAmount ,
          BasepriceUnitId ,
          BasepriceBaseAmount ,
          BasepriceBaseUnitId ,
          OverriddenGiftCardAmount ,
          MarkAsNew ,
          MarkAsNewStartDateTimeUtc ,
          MarkAsNewEndDateTimeUtc
        )
SELECT Id ,
          Name ,
          ShortDescription ,
          FullDescription ,
          AdminComment ,
          ShowOnHomePage ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          AllowCustomerReviews ,
          ApprovedRatingSum ,
          NotApprovedRatingSum ,
          ApprovedTotalReviews ,
          NotApprovedTotalReviews ,
          Published ,
          Deleted ,
          CreatedOnUtc ,
          UpdatedOnUtc ,
          ProductTemplateId ,
          SubjectToAcl ,
          LimitedToStores ,
          VendorId ,
          ProductTypeId ,
          ParentGroupedProductId ,
          SKU ,
          ManufacturerPartNumber ,
          Gtin ,
          IsGiftCard ,
          GiftCardTypeId ,
          RequireOtherProducts ,
          RequiredProductIds ,
          AutomaticallyAddRequiredProducts ,
          IsDownload ,
          DownloadId ,
          UnlimitedDownloads ,
          MaxNumberOfDownloads ,
          DownloadExpirationDays ,
          DownloadActivationTypeId ,
          HasSampleDownload ,
          SampleDownloadId ,
          HasUserAgreement ,
          UserAgreementText ,
          IsRecurring ,
          RecurringCycleLength ,
          RecurringCyclePeriodId ,
          RecurringTotalCycles ,
          IsShipEnabled ,
          IsFreeShipping ,
          AdditionalShippingCharge ,
          IsTaxExempt ,
          TaxCategoryId ,
          ManageInventoryMethodId ,
          StockQuantity ,
          DisplayStockAvailability ,
          DisplayStockQuantity ,
          MinStockQuantity ,
          LowStockActivityId ,
          NotifyAdminForQuantityBelow ,
          BackorderModeId ,
          AllowBackInStockSubscriptions ,
          OrderMinimumQuantity ,
          OrderMaximumQuantity ,
          AllowedQuantities ,
          DisableBuyButton ,
          DisableWishlistButton ,
          AvailableForPreOrder ,
          CallForPrice ,
          Price ,
          OldPrice ,
          ProductCost ,
          SpecialPrice ,
          SpecialPriceStartDateTimeUtc ,
          SpecialPriceEndDateTimeUtc ,
          CustomerEntersPrice ,
          MinimumCustomerEnteredPrice ,
          MaximumCustomerEnteredPrice ,
          HasTierPrices ,
          HasDiscountsApplied ,
          [Weight] ,
          [Length] ,
          Width ,
          Height ,
          AvailableStartDateTimeUtc ,
          AvailableEndDateTimeUtc ,
          VisibleIndividually ,
          DisplayOrder ,
          PreOrderAvailabilityStartDateTimeUtc ,
          DeliveryDateId ,
          WarehouseId ,
          AllowAddingOnlyExistingAttributeCombinations ,
          ShipSeparately ,
          UseMultipleWarehouses ,
          IsRental ,
          RentalPriceLength ,
          RentalPricePeriodId ,
          IsTelecommunicationsOrBroadcastingOrElectronicServices ,
          BasepriceEnabled ,
          BasepriceAmount ,
          BasepriceUnitId ,
          BasepriceBaseAmount ,
          BasepriceBaseUnitId ,
          OverriddenGiftCardAmount ,
          MarkAsNew ,
          MarkAsNewStartDateTimeUtc ,
          MarkAsNewEndDateTimeUtc 
FROM [spheres1].dbo.Product

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT,
		@currentProductTemplateId INT,
		@currentVendorId INT,
		@currentDownloadId INT,
		@currentTaxCategoryId INT,
		@currentDeliveryDateId INT,
		@currentWarehouseId INT


SELECT @currentElementId = Id,
		@currentProductTemplateId = ProductTemplateId,
		@currentVendorId =VendorId,
		@currentDownloadId = DownloadId,
		@currentTaxCategoryId =TaxCategoryId,
		@currentDeliveryDateId =DeliveryDateId,
		@currentWarehouseId = WarehouseId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT,
			@currentProductTemplateIdInNewTable INT,
			@currentVendorIdInNewTable INT,
			@currentDownloadIdInNewTable INT,
			@currentTaxCategoryIdInNewTable INT,
			@currentDeliveryDateIdInNewTable INT,
			@currentWarehouseIdInNewTable INT
	
	---GET PRODUCT TEMPLATE
	SELECT @currentProductTemplateIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentProductTemplateId AND TableName = 'ProductTemplate'
	IF (@currentProductTemplateIdInNewTable IS NULL) AND (@currentProductTemplateId IS NOT NULL)
		PRINT 'New ProductTemplate is not found'

	---GET VENDOR
	SELECT @currentVendorIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentVendorId AND TableName = 'Vendor'
	IF (@currentVendorIdInNewTable IS NULL) AND (@currentVendorId IS NOT NULL)
		PRINT 'New Vendor is not found'

	---GET DOWNLOADS
	SELECT @currentDownloadIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentDownloadId AND TableName = 'Download'
	IF (@currentDownloadIdInNewTable IS NULL) AND (@currentDownloadId IS NOT NULL)
		SET @currentDownloadIdInNewTable = @currentDownloadId

	---GET TAX CATEGORY
	SELECT @currentTaxCategoryIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentTaxCategoryId AND TableName = 'TaxCategory'
	IF (@currentTaxCategoryIdInNewTable IS NULL) AND (@currentTaxCategoryId IS NOT NULL)
		SET @currentTaxCategoryIdInNewTable = @currentTaxCategoryId

	---GET DELIVERY DATE
	SELECT @currentDeliveryDateIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentDeliveryDateId AND TableName = 'TaxCategory'
	IF (@currentDeliveryDateIdInNewTable IS NULL) AND (@currentDeliveryDateId IS NOT NULL)
		PRINT 'New DeliveryDate is not found'

	---GET WAREHOUSE
	SELECT @currentWarehouseIdInNewTable = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentWarehouseId AND TableName = 'Warehouse'
	IF (@currentWarehouseIdInNewTable IS NULL) AND (@currentWarehouseId IS NOT NULL)
		PRINT 'New Warehouse is not found'


	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE
	INSERT INTO [hpl2].dbo.Product
	        ( Name ,
	          ShortDescription ,
	          FullDescription ,
	          AdminComment ,
	          ShowOnHomePage ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          AllowCustomerReviews ,
	          ApprovedRatingSum ,
	          NotApprovedRatingSum ,
	          ApprovedTotalReviews ,
	          NotApprovedTotalReviews ,
	          Published ,
	          Deleted ,
	          CreatedOnUtc ,
	          UpdatedOnUtc ,
	          ProductTemplateId ,
	          SubjectToAcl ,
	          LimitedToStores ,
	          VendorId ,
	          ProductTypeId ,
	          ParentGroupedProductId ,
	          SKU ,
	          ManufacturerPartNumber ,
	          Gtin ,
	          IsGiftCard ,
	          GiftCardTypeId ,
	          RequireOtherProducts ,
	          RequiredProductIds ,
	          AutomaticallyAddRequiredProducts ,
	          IsDownload ,
	          DownloadId ,
	          UnlimitedDownloads ,
	          MaxNumberOfDownloads ,
	          DownloadExpirationDays ,
	          DownloadActivationTypeId ,
	          HasSampleDownload ,
	          SampleDownloadId ,
	          HasUserAgreement ,
	          UserAgreementText ,
	          IsRecurring ,
	          RecurringCycleLength ,
	          RecurringCyclePeriodId ,
	          RecurringTotalCycles ,
	          IsShipEnabled ,
	          IsFreeShipping ,
	          AdditionalShippingCharge ,
	          IsTaxExempt ,
	          TaxCategoryId ,
	          ManageInventoryMethodId ,
	          StockQuantity ,
	          DisplayStockAvailability ,
	          DisplayStockQuantity ,
	          MinStockQuantity ,
	          LowStockActivityId ,
	          NotifyAdminForQuantityBelow ,
	          BackorderModeId ,
	          AllowBackInStockSubscriptions ,
	          OrderMinimumQuantity ,
	          OrderMaximumQuantity ,
	          AllowedQuantities ,
	          DisableBuyButton ,
	          DisableWishlistButton ,
	          AvailableForPreOrder ,
	          CallForPrice ,
	          Price ,
	          OldPrice ,
	          ProductCost ,
	          SpecialPrice ,
	          SpecialPriceStartDateTimeUtc ,
	          SpecialPriceEndDateTimeUtc ,
	          CustomerEntersPrice ,
	          MinimumCustomerEnteredPrice ,
	          MaximumCustomerEnteredPrice ,
	          HasTierPrices ,
	          HasDiscountsApplied ,
	          [Weight] ,
	          [Length] ,
	          Width ,
	          Height ,
	          AvailableStartDateTimeUtc ,
	          AvailableEndDateTimeUtc ,
	          VisibleIndividually ,
	          DisplayOrder ,
	          PreOrderAvailabilityStartDateTimeUtc ,
	          DeliveryDateId ,
	          WarehouseId ,
	          AllowAddingOnlyExistingAttributeCombinations ,
	          ShipSeparately ,
	          UseMultipleWarehouses ,
	          IsRental ,
	          RentalPriceLength ,
	          RentalPricePeriodId ,
	          IsTelecommunicationsOrBroadcastingOrElectronicServices ,
	          BasepriceEnabled ,
	          BasepriceAmount ,
	          BasepriceUnitId ,
	          BasepriceBaseAmount ,
	          BasepriceBaseUnitId ,
	          OverriddenGiftCardAmount ,
	          MarkAsNew ,
	          MarkAsNewStartDateTimeUtc ,
	          MarkAsNewEndDateTimeUtc
	        )	
	SELECT  Name ,
	          ShortDescription ,
	          FullDescription ,
	          AdminComment ,
	          ShowOnHomePage ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          AllowCustomerReviews ,
	          ApprovedRatingSum ,
	          NotApprovedRatingSum ,
	          ApprovedTotalReviews ,
	          NotApprovedTotalReviews ,
	          Published ,
	          Deleted ,
	          CreatedOnUtc ,
	          UpdatedOnUtc ,
	          @currentProductTemplateIdInNewTable , --TEMPLATE
	          SubjectToAcl ,
	          1 , --LIMITED TO STORE
	          @currentVendorIdInNewTable , --VENDOR
	          ProductTypeId ,
	          ParentGroupedProductId ,
	          SKU ,
	          ManufacturerPartNumber ,
	          Gtin ,
	          IsGiftCard ,
	          GiftCardTypeId ,
	          RequireOtherProducts ,
	          RequiredProductIds ,
	          AutomaticallyAddRequiredProducts ,
	          IsDownload ,
	          @currentDownloadId , --DOWNLOAD
	          UnlimitedDownloads ,
	          MaxNumberOfDownloads ,
	          DownloadExpirationDays ,
	          DownloadActivationTypeId , 
	          HasSampleDownload ,
	          SampleDownloadId ,
	          HasUserAgreement ,
	          UserAgreementText ,
	          IsRecurring ,
	          RecurringCycleLength ,
	          RecurringCyclePeriodId ,
	          RecurringTotalCycles ,
	          IsShipEnabled ,
	          IsFreeShipping ,
	          AdditionalShippingCharge ,
	          IsTaxExempt ,
	          @currentTaxCategoryIdInNewTable , --TAX CATEGORY
	          ManageInventoryMethodId ,
	          StockQuantity ,
	          DisplayStockAvailability ,
	          DisplayStockQuantity ,
	          MinStockQuantity ,
	          LowStockActivityId ,
	          NotifyAdminForQuantityBelow ,
	          BackorderModeId ,
	          AllowBackInStockSubscriptions ,
	          OrderMinimumQuantity ,
	          OrderMaximumQuantity ,
	          AllowedQuantities ,
	          DisableBuyButton ,
	          DisableWishlistButton ,
	          AvailableForPreOrder ,
	          CallForPrice ,
	          Price ,
	          OldPrice ,
	          ProductCost ,
	          SpecialPrice ,
	          SpecialPriceStartDateTimeUtc ,
	          SpecialPriceEndDateTimeUtc ,
	          CustomerEntersPrice ,
	          MinimumCustomerEnteredPrice ,
	          MaximumCustomerEnteredPrice ,
	          HasTierPrices ,
	          HasDiscountsApplied ,
	          [Weight] ,
	          [Length] ,
	          Width ,
	          Height ,
	          AvailableStartDateTimeUtc ,
	          AvailableEndDateTimeUtc ,
	          VisibleIndividually ,
	          DisplayOrder ,
	          PreOrderAvailabilityStartDateTimeUtc ,
	          @currentDeliveryDateIdInNewTable , -- DELIVARY DATE
	          @currentWarehouseIdInNewTable , -- WAREHOUSE
	          AllowAddingOnlyExistingAttributeCombinations ,
	          ShipSeparately ,
	          UseMultipleWarehouses ,
	          IsRental ,
	          RentalPriceLength ,
	          RentalPricePeriodId ,
	          IsTelecommunicationsOrBroadcastingOrElectronicServices ,
	          BasepriceEnabled ,
	          BasepriceAmount ,
	          BasepriceUnitId ,
	          BasepriceBaseAmount ,
	          BasepriceBaseUnitId ,
	          OverriddenGiftCardAmount ,
	          MarkAsNew ,
	          MarkAsNewStartDateTimeUtc ,
	          MarkAsNewEndDateTimeUtc FROM @currentElement
		
	SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	---UPDATE STORE MAPPING
	INSERT INTO [hpl2].dbo.StoreMapping ( EntityId, EntityName, StoreId )
	VALUES ( @currentElementIdInNewTable, @TableName, @CurrentStoreId )

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'