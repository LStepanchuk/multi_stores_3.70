USE spheres1
DECLARE @TableName varchar(100) = 'Topic';  
DECLARE @CurrentStore INT = 6;

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END

PRINT N'START MIGRATION [' + @TableName + ']'

DECLARE @AllElements TABLE
(
	[Id] [INT],
	[SystemName] [NVARCHAR](MAX),
	[IncludeInSitemap] [BIT],
	[IsPasswordProtected] [BIT],
	[Password] [NVARCHAR](MAX),
	[Title] [NVARCHAR](MAX),
	[Body] [NVARCHAR](MAX),
	[MetaKeywords] [NVARCHAR](MAX),
	[MetaDescription] [NVARCHAR](MAX),
	[MetaTitle] [NVARCHAR](MAX),
	[Order] [INT],
	[DisplayOnInformationBar] [BIT],
	[LimitedToStores] [BIT],
	[IncludeInTopMenu] [BIT],
	[TopicTemplateId] [INT],
	[IncludeInFooterColumn1] [BIT],
	[IncludeInFooterColumn2] [BIT],
	[IncludeInFooterColumn3] [BIT],
	[AccessibleWhenStoreClosed] [BIT],
	[DisplayOrder] [INT],
	[SubjectToAcl] [BIT]
);

DECLARE @currentElement TABLE
(
	[Id] [INT],
	[SystemName] [NVARCHAR](MAX),
	[IncludeInSitemap] [BIT],
	[IsPasswordProtected] [BIT],
	[Password] [NVARCHAR](MAX),
	[Title] [NVARCHAR](MAX),
	[Body] [NVARCHAR](MAX),
	[MetaKeywords] [NVARCHAR](MAX),
	[MetaDescription] [NVARCHAR](MAX),
	[MetaTitle] [NVARCHAR](MAX),
	[Order] [INT],
	[DisplayOnInformationBar] [BIT],
	[LimitedToStores] [BIT],
	[IncludeInTopMenu] [BIT],
	[TopicTemplateId] [INT],
	[IncludeInFooterColumn1] [BIT],
	[IncludeInFooterColumn2] [BIT],
	[IncludeInFooterColumn3] [BIT],
	[AccessibleWhenStoreClosed] [BIT],
	[DisplayOrder] [INT],
	[SubjectToAcl] [BIT]
);

INSERT INTO @AllElements
        ( Id ,
          SystemName ,
          IncludeInSitemap ,
          IsPasswordProtected ,
          [Password] ,
          Title ,
          Body ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          [Order] ,
          DisplayOnInformationBar ,
          LimitedToStores ,
          IncludeInTopMenu ,
          TopicTemplateId ,
          IncludeInFooterColumn1 ,
          IncludeInFooterColumn2 ,
          IncludeInFooterColumn3 ,
          AccessibleWhenStoreClosed ,
          DisplayOrder ,
          SubjectToAcl
        )
SELECT Id ,
          SystemName ,
          IncludeInSitemap ,
          IsPasswordProtected ,
          [Password] ,
          Title ,
          Body ,
          MetaKeywords ,
          MetaDescription ,
          MetaTitle ,
          DisplayOrder ,
          IncludeInTopMenu ,
          LimitedToStores ,
          IncludeInTopMenu ,
          TopicTemplateId ,
          IncludeInFooterColumn1 ,
          IncludeInFooterColumn2 ,
          IncludeInFooterColumn3 ,
          AccessibleWhenStoreClosed ,
          DisplayOrder ,
          SubjectToAcl
FROM [spheres1].dbo.Topic

PRINT N'START LOOP';
WHILE EXISTS (SELECT * FROM @AllElements)
BEGIN
	---GET FIRST ELEMENT
	DELETE FROM @currentElement
	INSERT INTO @currentElement
	SELECT TOP 1 * FROM @AllElements

	---CURRENT ELEMENT FIELDS
	DECLARE @currentElementId INT,
			@currentTopicTemplateId INT

	---CURRENT ELEMENT FIELDS THAT ALREADY MIGRATED
	DECLARE @currentElementId_InNewDB INT,
			@currentTopicTemplateId_InNewDB INT

	SELECT  @currentElementId = Id,
		    @currentTopicTemplateId = TopicTemplateId
	FROM    @currentElement
	
	---MAIN SECTION	

	---GET TOPIC TEMPLATE
	SELECT @currentTopicTemplateId_InNewDB = [NewId] FROM dbo.OldElementsStorage
	WHERE OldId = @currentTopicTemplateId AND TableName = 'TopicTemplate'
	IF (@currentTopicTemplateId_InNewDB IS NULL) AND (@currentTopicTemplateId IS NOT NULL)
		PRINT 'New TopicTemplate is not found'

	INSERT INTO [hpl2].dbo.Topic
	        ( SystemName ,
	          IncludeInSitemap ,
	          IsPasswordProtected ,
	          [Password] ,
	          Title ,
	          Body ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          [Order] ,
	          DisplayOnInformationBar ,
	          LimitedToStores ,
	          IncludeInTopMenu ,
	          TopicTemplateId ,
	          IncludeInFooterColumn1 ,
	          IncludeInFooterColumn2 ,
	          IncludeInFooterColumn3 ,
	          AccessibleWhenStoreClosed ,
	          DisplayOrder ,
	          SubjectToAcl
	        )
	SELECT	SystemName ,
	          IncludeInSitemap ,
	          IsPasswordProtected ,
	          [Password] ,
	          Title ,
	          Body ,
	          MetaKeywords ,
	          MetaDescription ,
	          MetaTitle ,
	          [Order] ,
	          DisplayOnInformationBar ,
	          1 ,
	          IncludeInTopMenu ,
	          @currentTopicTemplateId_InNewDB ,
	          IncludeInFooterColumn1 ,
	          IncludeInFooterColumn2 ,
	          IncludeInFooterColumn3 ,
	          AccessibleWhenStoreClosed ,
	          DisplayOrder ,
	          SubjectToAcl
	FROM @currentElement
	
	SET @currentElementId_InNewDB = SCOPE_IDENTITY();
	
	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementId_InNewDB, @TableName)

	---UPDATE STORE MAPPING
	INSERT INTO [hpl2].dbo.StoreMapping ( EntityId, EntityName, StoreId )
	VALUES ( @currentElementId_InNewDB, @TableName, @CurrentStore )

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

PRINT N'MIGRATION FINISHED ['+ @TableName +']'