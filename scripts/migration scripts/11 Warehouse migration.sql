USE spheres1
DECLARE @TableName varchar(100) = 'Warehouse';

IF OBJECT_ID('dbo.OldElementsStorage', 'U') IS NULL 
BEGIN
  CREATE TABLE dbo.OldElementsStorage ([OldId] INT, [NewId] INT, [TableName] NVARCHAR(100));  
END
ELSE
BEGIN
  DELETE FROM dbo.OldElementsStorage
  WHERE TableName = @TableName
END
INSERT INTO dbo.OldElementsStorage ( OldId, [NewId], TableName )  VALUES  ( 0, 0, @TableName )

PRINT N'START MIGRATION [' + @TableName + ']'
BEGIN

DECLARE @AllElements TABLE
(	
	[Id] INT,
	[Name] NVARCHAR(400),
	[AddressId] INT,
	[AdminComment] NVARCHAR(MAX)
);

DECLARE @currentElement TABLE
(
	[Id] INT,
	[Name] NVARCHAR(400),
	[AddressId] INT,
	[AdminComment] NVARCHAR(MAX)
);

INSERT INTO @AllElements ( Id, Name, AddressId, AdminComment )

SELECT Id, Name, AddressId, AdminComment
FROM [spheres1].dbo.Warehouse

PRINT N'START LOOP';
 
StartLoop: --GOTO LABEL

---GET FIRST ELEMENT
DELETE FROM @currentElement
INSERT INTO @currentElement
SELECT TOP 1 * FROM @AllElements

DECLARE @currentElementId INT;
DECLARE @currentElementName NVARCHAR(400);
DECLARE @currentAddressId INT;

SELECT @currentElementId = Id,
@currentElementName = Name,
@currentAddressId = AddressId
FROM @currentElement

BEGIN
	DECLARE @currentElementIdInNewTable INT
		
	---IMPORTANT---MUST BE EDITED	
	---INSERT NEW ELEMENT INTO TARGET TABLE

	IF EXISTS(SELECT * FROM [hpl2].dbo.Warehouse WHERE Name = @currentElementName AND AddressId = @currentAddressId)
	BEGIN
		SELECT @currentElementIdInNewTable = Id FROM [hpl2].dbo.Warehouse 
		WHERE Name = @currentElementName AND AddressId = @currentAddressId
	END
	ELSE
	BEGIN
		---GET ADDRESS
		DECLARE @addressIdInNewTable INT;
		SELECT @addressIdInNewTable = [NewId] FROM dbo.OldElementsStorage 
		WHERE OldId = @currentAddressId AND TableName = 'Address'

		INSERT INTO [hpl2].dbo.Warehouse ( Name, AddressId, AdminComment )		
		SELECT  Name, AddressId, AdminComment FROM @currentElement
		
		SET @currentElementIdInNewTable = SCOPE_IDENTITY();
	END
	---IMPORTANT

	INSERT INTO dbo.OldElementsStorage ([OldId], [NewId], [TableName])
	VALUES (@currentElementId, @currentElementIdInNewTable, @TableName)

	DELETE FROM @AllElements
	WHERE Id = @currentElementId
END

IF EXISTS (SELECT * FROM @AllElements)
	GOTO StartLoop

END
PRINT N'MIGRATION FINISHED ['+ @TableName +']'