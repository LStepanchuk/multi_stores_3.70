USE [hpl2]

DECLARE @CurrentStoreId INT = 1;

INSERT INTO dbo.StoreMapping
        ( EntityId, EntityName, StoreId )
SELECT Id, 'Category', @CurrentStoreId FROM dbo.Category
UPDATE dbo.Category SET LimitedToStores = 1

INSERT INTO dbo.StoreMapping
        ( EntityId, EntityName, StoreId )
SELECT Id, 'Product', @CurrentStoreId FROM dbo.Product
UPDATE dbo.Product SET LimitedToStores = 1

INSERT INTO dbo.StoreMapping
        ( EntityId, EntityName, StoreId )
SELECT Id, 'Topic', @CurrentStoreId FROM dbo.Topic
UPDATE dbo.Topic SET LimitedToStores = 1

INSERT INTO dbo.StoreMapping
        ( EntityId, EntityName, StoreId )
SELECT Id, 'Manufacturer', @CurrentStoreId FROM dbo.Manufacturer
UPDATE dbo.Manufacturer SET LimitedToStores = 1
